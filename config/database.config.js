const loki = require('lokijs');

const DB_NAME = 'space.db';
const POINTS_COLLECTION = 'points';

// We will use autoload (one time load at instantiation), and autosave  with 4 sec interval.
_db = new loki(DB_NAME, {
    autoload: true,
    autoloadCallback : databaseInitialize,
    autosave: true,
    autosaveInterval: 4000 // save every four seconds for our example
});

// implement the autoloadback referenced in loki constructor
function databaseInitialize() {
    // on the first load of (non-existent database), we will have no collections so we can
    //   detect the absence of our collections and add (and configure) them now.
    const entries = _db.getCollection(POINTS_COLLECTION);
    if (entries === null) {
        _db.addCollection(POINTS_COLLECTION);
    }
}