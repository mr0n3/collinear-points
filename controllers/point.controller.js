const Point = require('../models/point.model.js');

const POINTS_COLLECTION = 'points';

const validatePoint = (x, y) => {
    let noError = {};

    if (!x || !y) { // Check input valorization
        return {message: "A point with x and y coordinates is required."};
    }

    if (!(typeof x === 'number')) { // check x value type
        return {message: "x must be a number."};
    }

    if (!(typeof y === 'number')) { // check y value type
        return {message: "y must be a number."};
    }

    const points = _db.getCollection(POINTS_COLLECTION);
    const results = points.find({'x': x, 'y': y});
    if (results.length > 0) { // check exist
        return {message: "Point already exist in space."};
    }

    return noError;
};

exports.create = function (req, res) {
    const x = req.body ? req.body.x : null;
    const y = req.body ? req.body.y : null;

    const error = validatePoint(x, y);

    if (error.message === undefined) {
        const points = _db.getCollection(POINTS_COLLECTION);
        points.insert(new Point(x, y));
        res.status(201).send({message: "Point created."});
    } else {
        res.status(400).send(error);
    }
};

exports.findAll = function (req, res) {
    const points = _db.getCollection(POINTS_COLLECTION);
    res.status(200).json(points.data);
};

exports.getLinesInSpace = function (req, res) {
    const points = req.params.points;

    //TODO algorithm

    res.status(200).send({message: "ok"});
};

exports.deleteAll = function (req, res) {
    const points = _db.getCollection(POINTS_COLLECTION);

    points.clear();
    res.status(200).send({message: "All points deleted."});
};