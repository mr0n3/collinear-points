class Point {
    constructor(x = null, y = null) {
        this.x = x;
        this.y = y;
    }
}

module.exports = Point;