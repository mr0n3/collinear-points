const express = require('express');
const bodyParser = require('body-parser');

const pointController = require('./controllers/point.controller.js');

require('./config/database.config');

// create express app
const app = express();

app.use(bodyParser.json());

// routes
app.post('/point', pointController.create);

app.get('/space', pointController.findAll);

app.get('/lines/:points', pointController.getLinesInSpace);

app.delete('/space', pointController.deleteAll);

app.listen(3000, () => console.log("Startup server - listening on port 3000"));